package ppa.labs.commons.csv.engine;

import org.apache.commons.csv.CSVRecord;
import ppa.labs.commons.csv.enums.HeaderEnum;
import ppa.labs.commons.csv.model.Person;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class CSVPersonEngineImpl implements CSVPersonEngine {
    @Override
    public Reader readCSVPersonFile(Path csvPersonPath) throws FileNotFoundException {
        return new FileReader(csvPersonPath.toString());
    }

    @Override
    public Map<Integer, Person> extractPersons(Reader in) throws IOException {
        Iterable<CSVRecord> linesPerson = this.extractsPersonLines(in);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Map<Integer, Person> persons = new HashMap<>();
        AtomicInteger order = new AtomicInteger();

        for (CSVRecord linePerson : linesPerson) {
            if(order.getAndIncrement() == 0) {
                continue;
            }

            Person person = new Person(Integer.parseInt(linePerson.get(HeaderEnum.ID)),
                    linePerson.get(HeaderEnum.FIRSTNAME),
                    linePerson.get(HeaderEnum.LASTNAME),
                    LocalDate.parse(linePerson.get(HeaderEnum.BIRTHDATE), formatter),
                    linePerson.get(HeaderEnum.FUNCTION));
            persons.put(order.getAndIncrement(), person);

        }
        return persons;
    }
}
