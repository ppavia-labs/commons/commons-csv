package ppa.labs.commons.csv.engine;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import ppa.labs.commons.csv.enums.HeaderEnum;
import ppa.labs.commons.csv.model.Person;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Path;
import java.util.Map;

public interface CSVPersonEngine {

    Reader readCSVPersonFile (Path csvPersonPath) throws FileNotFoundException;

    default Iterable<CSVRecord> extractsPersonLines (Reader in) throws IOException {
        return CSVFormat.RFC4180.withDelimiter(';').withHeader(HeaderEnum.class).parse(in);
    }

    Map<Integer, Person> extractPersons(Reader in) throws IOException;
}
