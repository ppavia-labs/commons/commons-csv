package ppa.labs.commons.csv;

import ppa.labs.commons.csv.engine.CSVPersonEngine;
import ppa.labs.commons.csv.engine.CSVPersonEngineImpl;
import ppa.labs.commons.csv.model.Person;
import ppa.labs.commons.csv.service.CSVPersonService;
import ppa.labs.commons.csv.service.CSVPersonServiceImpl;

import java.nio.file.Path;
import java.util.Map;

public class CSVMain {

    public static void main(String... args) {
        CSVPersonEngine personEngine = new CSVPersonEngineImpl();
        CSVPersonService personService = new CSVPersonServiceImpl(personEngine);
        Path csvPath = Path.of("src/main/resources/persons.csv");
        Map<Integer, Person> persons = personService.getPersons(csvPath);

        persons.forEach((order, person) -> {
            System.out.println(order + " " + person.getFirstName() + " " + person.getLastName());
        });
    }
}
