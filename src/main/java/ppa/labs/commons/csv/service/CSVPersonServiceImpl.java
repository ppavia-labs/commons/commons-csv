package ppa.labs.commons.csv.service;

import org.apache.commons.csv.CSVRecord;
import ppa.labs.commons.csv.engine.CSVPersonEngine;
import ppa.labs.commons.csv.model.Person;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class CSVPersonServiceImpl implements CSVPersonService {

    private CSVPersonEngine csvPersonEngine;

    public CSVPersonServiceImpl(CSVPersonEngine csvPersonEngine) {
        this.csvPersonEngine = csvPersonEngine;
    }

    @Override
    public Map<Integer, Person> getPersons(Path pathToCSVPersonFile) {
        // get Reader from csv file
        Map<Integer, Person> persons = new HashMap<>();

        // get listed persons
        try {
            Reader reader = csvPersonEngine.readCSVPersonFile(pathToCSVPersonFile);
            persons = csvPersonEngine.extractPersons(reader);
        }
        catch (FileNotFoundException e) {
            System.err.println("ERREUR dans la récupération du fichier " + pathToCSVPersonFile);
            e.getStackTrace();
        }
        catch (IOException e) {
            System.err.println("ERREUR dans le traitement du fichier " + pathToCSVPersonFile);
            e.getStackTrace();
        }

        return persons;
    }
}
