package ppa.labs.commons.csv.service;

import ppa.labs.commons.csv.model.Person;

import java.nio.file.Path;
import java.util.Map;

public interface CSVPersonService {

    Map<Integer, Person> getPersons(Path pathToCSVPersonFile);
}
