package ppa.labs.commons.csv.enums;

public enum HeaderEnum {
    ID,
    FIRSTNAME,
    LASTNAME,
    BIRTHDATE,
    FUNCTION;
}
